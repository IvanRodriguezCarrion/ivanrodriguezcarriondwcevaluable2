var noticiasDesordenadas = [];

function buscar(titulo,fechaIni,fechaFin) { //El script para pedir la información a la API la proporciona la misma API
    noticiasDesordenadas = []; //reinicializamos para evitar acumulaciones
    var url = "https://api.nytimes.com/svc/search/v2/articlesearch.json";
    url += '?' + $.param({
        'api-key': "f2b8c2a7df7848d1bd474eb313f683d3",
        'q': titulo,
        'begin_date': fechaIni,
        'end_date': fechaFin
    });
    
    $.ajax({ // No le hacemos un parse al resultado porque ya viene en formato de objetos JSON.
        url: url,
        method: 'GET',
    }).done(function(result) {
        for (var i=0; i<result.response.docs.length; i++) {
            noticiasDesordenadas.push([formatearFecha(result.response.docs[i].pub_date), result.response.docs[i].headline.main, result.response.docs[i].web_url]);
        }
        
        if ($("#cabecera").hasClass("panel-danger")) {
            $("#cabecera").removeClass("panel panel-danger").addClass("panel panel-primary");
        }
        $("#noticias").empty();
        muestraNoticias();
    }).fail(function(jqXHR, textStatus, errorThrown) {
        $("#cabecera").removeClass("panel panel-primary").addClass("panel panel-danger");
        $("#noticias").html("<li>No se ha podido realizar la consulta:"+jqXHR.status+"</li>");
        throw err;
    });
}

function muestraNoticias() {
    var noticiasOrdenadas = [];
    
    switch ($("#ordenar option:checked").val()) {
        case "1":
            noticiasOrdenadas = ordenarPorFecha(noticiasDesordenadas, "ASC");
            break;
        case "2":
            noticiasOrdenadas = ordenarPorFecha(noticiasDesordenadas, "DES");
            break;
    }
    
    $("#noticias").empty();
    
    for (var i=0; i<noticiasOrdenadas.length; i++) {
        $("#noticias").append("<li><strong>Fecha</strong>:"+reformatearFecha(noticiasOrdenadas[i][0])+" <strong>Noticia</strong>: " + noticiasOrdenadas[i][1] + " <strong>URL</strong>: <a href="+noticiasOrdenadas[i][2]+">" + noticiasOrdenadas[i][2]+"</a>");
    }
}

function formatearFecha(fecha) {
    var fechaRecu = fecha.split("-"); 
    // devuelve los 8 numeros de la fecha ignorando lo que hay detrás del símbolo T: 2017-02-13T05:00:00+0000 -> 20170213.
    fecha = fechaRecu[0]+fechaRecu[1]+fechaRecu[2];
    return parseInt(fecha);
}

function ordenarPorFecha(noticias, tipoOrden) {
    if (tipoOrden==="ASC") {
        noticias.sort(function (a, b) {
            return a[0]-b[0]; // ordenacion de menor a mayor por el elemento[0] del array (fecha)
        });
    } else if(tipoOrden==="DES") {
        noticias.sort(function (a, b) {
            return b[0]-a[0];// ordenacion de mayor a menor por el elemento[0] del array (fecha)
        });
    }
    return noticias;
}

function comprobarFecha() {
    var fechaIni=parseInt($("#fechaInicio").val());
    var fechaFin=parseInt($("#fechaFinal").val());
    
    if($.isNumeric($("#fechaInicio").val()) && $.isNumeric($("#fechaFinal").val()) ) { // comprobacion de si es un numero
        if(fechaIni<10000000 || fechaIni>=100000000) { //comprobacion si tiene 8 dígitos
            alert("La fecha de inicio no es correcta: 8 dígitos YYYYMMDD.");
            return false;
        } else if (fechaFin<10000000 || fechaFin>=100000000) {
            alert("La fecha final no es correcta: 8 dígitos YYYYMMDD.");
            return false;
        }
    } else {
        alert("Alguna de las fechas no es numérica.");
        return false;
    }
    
    if (fechaFin - fechaIni < 0) {
        alert("La fecha de inicio es mayor que la de fin");
        return false;
    }
    return true;
}

function reformatearFecha(fecha) { //devuelve una fecha legible
    fecha = fecha.toString(); 
    var fechaCambiada = fecha[6]+fecha[7]+"-"+fecha[4]+fecha[5]+"-"+fecha[0]+"."+fecha[1]+fecha[2]+fecha[3];
    return fechaCambiada;
}

function comprobarCampos() {
    if ($("#titulo").val()=="" || $("#fechaInicio").val()=="" || $("#fechaFinal").val()=="") {
        alert("Alguno de los campos está vacío");
        return false;
    }
    return true;
}

$(function () { // funcion onload
    $("#enviar").click(function (){
        if(comprobarCampos() && comprobarFecha()){
            buscar($("#titulo").val(),$("#fechaInicio").val(),$("#fechaFinal").val());
        }
    });
    
    $("#ordenar").change(muestraNoticias);
});


