var equipos = [];
var equiposPuntos = [];
var equiposGoles = [];

function buscar() { //metodo con el que recuperamos el elemento JSON y lo pasamos a vector
    equipos = [];
    equiposPuntos = [];
    equiposGoles = [];
    var url = "http://hispabyte.net/DWEC/entregable2-2.php";
    $.ajax({
        url: url,
        method: 'GET',
    }).done(function(result) {
        result = JSON.parse(result);
        for (var i=0; i<result.length; i++) { // tres vectores distintos para poder tratarlos de forma independiente y que no haya conflictos.
            equipos.push([result[i].nombre,parseInt(result[i].puntos),parseInt(result[i].golesAFavor)]);
            equiposPuntos.push([result[i].nombre,parseInt(result[i].puntos),parseInt(result[i].golesAFavor)]);
            equiposGoles.push([result[i].nombre,parseInt(result[i].puntos),parseInt(result[i].golesAFavor)]);
        }
        muestraEquipos();
    }).fail(function(err) {
        throw err;
    });
}

function muestraEquipos() {
    var equiposOrdenados = [];
    
    $("#equipos").empty();
    
    switch ($("#ordenar option:checked").val()) { //reordenación según interés
        case "1":
            equiposOrdenados = equipos; //original
            break;
        case "2":
            equiposOrdenados = ordenar(equiposPuntos, "puntos");
            break;
        case "3":
            equiposOrdenados = ordenar(equiposGoles, "goles");
            break;    
    }
    
    for (var i=0; i<equiposOrdenados.length; i++) {
        $("#equipos").append("<li><strong>Nombre</strong>:"+equiposOrdenados[i][0]+" <strong>Puntos</strong>: " + equiposOrdenados[i][1] + " <strong>Goles a favor</strong>: "+equiposOrdenados[i][2]);
    }
}

function ordenar(equiposDesordenados, tipoOrden) {
    
    if (tipoOrden==="puntos") { //ordenar por puntos guardados en la posición 1 del vector
        equiposDesordenados.sort(function (a, b) {
            return b[1]-a[1];
        });
    } else if(tipoOrden==="goles") { //ordenar por goles guardados en la posición 2 del vector
        equiposDesordenados.sort(function (a, b) {
            return b[2]-a[2];
        });
    }
    return equiposDesordenados;
}



$(function () { //funcion onload que pone los lissener sobre el botón y el combobox.
    $("#enviar").click(function (){
        buscar();
    });
    
    $("#ordenar").change(muestraEquipos);
});
