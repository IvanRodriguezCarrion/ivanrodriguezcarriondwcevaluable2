var productos = [];

function recuperar() {
    productos = [];
    var url = "http://hispabyte.net/DWEC/entregable2-3.php";
    $.ajax({ //metodo para recuperar la información vía ajax
        url: url,
        method: 'GET',
    }).done(function(result) {
        
        result = JSON.parse(result);
        
        for (var i=0; i<result.length; i++) {
            productos.push([parseInt(result[i].id), result[i].nombre, parseInt(result[i].cat), parseInt(result[i].precio), parseInt(result[i].unidades)]);  
        }
        productos.sort(function(a,b){ //reordenación por categoría
            return b[2]-a[2];
        });
        
        muestraProductos();
        
    }).fail(function(err) {
        throw err;
    });
}

function muestraProductos() {
//En función de si tenemos una tienda previa abierta la podemos cargar desde el localstorage. productos recupera el estado actual de la tienda productosOriginal su estado original (desde el último inicio de sesion)

    if (localStorage.getItem("productos")) {
        $("#productos").append(localStorage.getItem("productos"));
    } else if (localStorage.getItem("productosOriginal")){
        $("#productos").append(localStorage.getItem("productosOriginal"));
    } else {
       for (var i=0; i<productos.length; i++) {
            $("#productos").append("<tr><td>"+productos[i][0]+"</td><td>"+productos[i][1]+"</td><td>" + productos[i][2] + "</td><td>"+productos[i][3]+"</td><td id='cantidadAlmacen"+i+"'>"+productos[i][4]+"</td><td><input type='text' id='cnt"+i+"' placeholder='cantidad'/> <input type='button' id='btn"+i+"' value='Añadir al carrito' data-id="+i+" /></tr>");
        } //En este punto guardamos la configuracion original de la tienda
        localStorage.setItem("productosOriginal",$("#productos").html());
    }
    
    //recuperamos lo que hubiera en el carrito
    $("#carrito").append(localStorage.getItem("carrito"));
    
    
    $('input[data-id]').each(function() {
         $(this).click(function(){
           var dataId = $(this).attr("data-id");
           var valorCaja = $('#cnt'+dataId).val();
           anadirCarrito(valorCaja,dataId);
            $('#cnt'+dataId).val(" ");
        });
    });

    
    $("#borrar").on("click", function(){
        $('input[type=text]').val(" ");
        $('input[type=button]').unbind(); // MUY imporante para no duplicar eventos al volver a llamar a esta función
        $("#carrito").empty();
        $("#productos").empty();
        localStorage.removeItem("carrito");
        localStorage.removeItem("productos");
        muestraProductos();
        //aqui se llama a si misma para regenerar las cantidades originales de productos liberados del carrito y, muy importante, recolocar los listener en cada uno de los botones que ha tenido que reinsertar.
    });
    
    $("#recuperar").on("click", function(){ //con el unbind eliminamos los eventos asociados a los botones para que no se repita la información al volver a crearlos cuando recuperamos la tienda
        $('input[type=button]').unbind();
        $("#productos").empty();
        $("#carrito").empty();
        localStorage.removeItem("carrito");
        localStorage.removeItem("productosOriginal");
        localStorage.removeItem("productos");
        recuperar();
    });
    
}

function anadirCarrito(cantidad,objeto) {
   
    if($.isNumeric(cantidad)) {
       
       var existencias = "#cantidadAlmacen"+objeto;
       var diferencia = parseInt($(existencias).html()) - parseInt(cantidad);
       
       if (diferencia >= 0) {
            
           var total = cantidad*productos[objeto][3];
           $(existencias).html(diferencia);
           
           $("#carrito").append("<tr><td>"+productos[objeto][0]+" </td><td>"+productos[objeto][1]+" </td><td> "+productos[objeto][3]+"€</td><td>"+cantidad+"</td> ><td>"+total+"€</td></tr>");
           
            localStorage.setItem("carrito",$("#carrito").html()); // Estado del carrito
            localStorage.setItem("productos",$("#productos").html()); //Estado de la tienda
           
       } else {
           alert("No hay suficientes unidades");
       }
    } else {
           alert("El valor introducido no es numérico");
       }
}

$(function () {
    recuperar();
});